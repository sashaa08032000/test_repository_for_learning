public abstract class GeomFigure {

    Double perimeter;
    Double area;

    public abstract Double getPerimeter();

    public abstract Double getArea();


}

