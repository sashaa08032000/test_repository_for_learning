import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Polygon extends GeomFigure implements WithAngles {

    private Double perimeter = (double) 0;
    private HashMap<Double,Double> coordinates = new HashMap<>();
    private ArrayList<Double> polygons = new ArrayList<>();

    public Polygon(Double ... polygons){
        this.polygons.addAll(List.of(polygons));
    }

    public ArrayList<Double> getPolygons() {
        return polygons;
    }

    @Override
    public Double getPerimeter(){
        for (double i :
                this.polygons) {
            this.perimeter += i;
        }
        return this.perimeter;
    }

    @Override
    public Double getArea() {
        return area;
    }

    public HashMap<Double, Double> getCoordinates() {
        return coordinates;
    }

    @Override
    public void setCoordinates(Double ... angle) {

        for (int x = 0; x< angle.length-1; x=x+2){
            this.coordinates.put(angle[x],angle[x+1]);
        }
    }

}
