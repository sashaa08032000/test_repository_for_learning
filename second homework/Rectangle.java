import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class Rectangle extends GeomFigure {

    private String description;
    private String skin;
    private Integer height;
    private Integer length;

    public Rectangle(Integer @NotNull ... length) {

        boolean squareness;
        if (length.length == 1) {
            this.length = length[0];
            this.height = this.length;
            squareness = true;
        } else if (length.length == 2) {
            this.length = length[0];
            this.height = length[1];
            squareness = false;
        } else {
            System.out.println("You made a mistake, select a different shape or reenter values");
        }
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setSkin(String skin) {
        this.skin = skin;
    }

    public void printDescription() {
        System.out.println(this.description);
    }

    public void printSkin() {
        System.out.println(this.skin);
    }

    public void increase(Integer x) {
        this.length = this.length * x;
        this.height = this.height * x;
        System.out.println("The lengths of the sides are successfully increased by " + x + " times\n");
    }

    @Override
    public Double getPerimeter() {
        this.perimeter = (double) ((this.length + this.height) * 2);
        return this.perimeter;
    }

    @Override
    public Double getArea() {
        this.area = this.length * this.area;
        return this.area;
    }

}
