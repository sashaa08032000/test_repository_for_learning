import java.util.ArrayList;
import java.util.List;

public class Rhombus extends Polygon implements EqualSides{

    private Boolean equal;

    private ArrayList<Double> polygons = new ArrayList<>();

    private Integer inscribedCircleRadius;

    public Rhombus(Integer inscribedCircleRadius, Boolean equal, Double ... polygons) {
        super(polygons);
        this.equal = equal;
        this.inscribedCircleRadius = inscribedCircleRadius;
    }

    public Boolean getEqual() {
        return equal;
    }

    public void setEqual(Boolean equal) {
        this.equal = equal;
    }

    public void setPolygons(ArrayList<Double> polygons) {
        this.polygons = polygons;
    }

    public void setInscribedCircleRadius(Integer inscribedCircleRadius) {
        this.inscribedCircleRadius = inscribedCircleRadius;
    }

    @Override
    public ArrayList<Double> getPolygons() {
        return polygons;
    }

    public Integer getInscribedCircleRadius() {
        return inscribedCircleRadius;
    }



    @Override
    public void setEqualSides(Boolean equal) {
        this.equal = equal;
    }

    @Override
    public Double getArea(){
        this.area = 2 * super.getPolygons().get(0) * this.inscribedCircleRadius;
        return area;
    }

}
