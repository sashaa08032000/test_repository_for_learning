import java.util.ArrayList;
import java.util.Arrays;

public class Trapezoid extends GeomFigure {
    private Boolean symmetry;
    private Boolean rectangular;
    private Integer height;
    private String description;
    private String skin;
    private ArrayList<Integer> polygons = new ArrayList<>();

    public Trapezoid(Integer height, Boolean symmetry, Boolean rectangular, Integer... length) {
        this.height = height;
        this.symmetry = symmetry;
        this.rectangular = rectangular;
        this.polygons.addAll(Arrays.asList(length));
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setSkin(String skin) {
        this.skin = skin;
    }

    public void increase(Integer x) {
        for (int i = 0; i < polygons.size(); i++) {
            this.polygons.set(i, this.polygons.get(i) * x);
        }
        System.out.println("The lengths of the sides are successfully increased by " + x + " times\n" + this.polygons);
    }

    public Boolean getSymmetry() {
        return symmetry;
    }

    public Boolean getRectangular() {
        return rectangular;
    }

    public Integer getHeight() {
        return height;
    }

    public ArrayList<Integer> getPolygons() {
        return polygons;
    }

    public void setSymmetry(Boolean symmetry) {
        this.symmetry = symmetry;
    }

    public void setRectangular(Boolean rectangular) {
        this.rectangular = rectangular;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public void setPolygons(ArrayList<Integer> polygons) {
        this.polygons = polygons;
    }

    @Override
    public Double getArea() {
        this.area = (double) ((this.polygons.get(0) + this.polygons.get(3)) / 2 * this.height);
        System.out.println("The area of the figure is calculated\nArea=" + this.area);
        return this.area;
    }

    @Override
    public Double getPerimeter(){
        for (int i :
                this.polygons) {
            this.perimeter += i;
        }
        System.out.println("The perimeter of the figure is calculated\nPerimeter=" + this.perimeter);
        return this.perimeter;
    }

    public void printMainProperties() {
        System.out.println("The main properties of:\n" +
                "\nLength: " + polygons +
                "\nIs it a symmetry?: " + symmetry +
                "\nIs it a rectangular?: " + symmetry +
                "\nIt's all");
    }


    private String getDescription() {
        return this.description;
    }

    private String getSkin() {
        return this.skin;
    }

}
