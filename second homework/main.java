public class Main {
    public static void main(String[] args) {
        Trapezoid first_figure = new Trapezoid(5, true, false, 2, 3, 3, 5);
        first_figure.area = 45.0;
        first_figure.perimeter = 12.0;
        System.out.println(first_figure.getArea());
        System.out.println(first_figure.getPerimeter());
        first_figure.printMainProperties();
        first_figure.increase(5);
        first_figure.getArea();
        first_figure.getPerimeter();
        System.out.println(first_figure.getArea());
        Rectangle second_figure = new Rectangle(2);
        second_figure.increase(5);
        Rhombus fourth_figure = new Rhombus(3,true, 4.0,5.0,6.0,7.0);
        fourth_figure.getPerimeter();
        System.out.println(fourth_figure.getArea());
    }
}